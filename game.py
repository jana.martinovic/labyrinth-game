from typing import List, Set
from labyrinth import Grid
from pixels import get_wall_pixels, get_rect_pixels
import pygame
import sys
import os


def resource_path(relative_path):
    #needed for PyInstaller
    try:
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)

# game info
SCREEN_SIZE = 600
GRID_SIZE_START = 5
GRID_SIZE_EASY = 10
GRID_SIZE_MEDIUM = 15
GRID_SIZE_HARD = 25
WALL_SIZE = 2
TIME_DELAY = 20

# init game + info
pygame.init()
pygame.display.set_caption("Labyrinth")
win = pygame.display.set_mode([SCREEN_SIZE, SCREEN_SIZE])
SCREEN_WIDTH, SCREEN_HEIGHT = pygame.display.get_surface().get_size()

# colors & fonts
BACKGROUND_C = (57, 43, 64)
TARGET_C = (163, 152, 123)
TRAIL_C = (150, 123, 163)
WALL_C = (203, 200, 154)
PLAYER_C = (162, 37, 167)
FONT_SIZE = 40
FONT_C = (255,255,255)
FONT = pygame.font.SysFont('Corbel', FONT_SIZE)

# buttons - mostly hardcoded, won't look good if screen size changed
BUTTON_W = SCREEN_WIDTH/5
BUTTON_H = SCREEN_HEIGHT/10
BUTTON_C = (163+50, 152+50, 123+50)

EASY_BUTTON_X = SCREEN_WIDTH/20
EASY_BUTTON_Y = SCREEN_HEIGHT*3/5
MED_BUTTON_X = SCREEN_WIDTH/20 + 1.5*BUTTON_W
MED_BUTTON_Y = SCREEN_HEIGHT*3/5
HARD_BUTTON_X = SCREEN_WIDTH/20 + 3.5*BUTTON_W
HARD_BUTTON_Y = SCREEN_HEIGHT*3/5

# player avatar info
PLAYER_W = 7 
PLAYER_H = 7 
VELOCITY = 4 #should not be greater than player width/height

# labyrinth info
grid = Grid(GRID_SIZE_START, GRID_SIZE_START).labyrinth()
gap = int((SCREEN_WIDTH-(grid.size()[0]*(WALL_SIZE-1)))/grid.size()[0])
target_x = (SCREEN_WIDTH - gap, SCREEN_WIDTH) 
target_y = (SCREEN_HEIGHT - gap, SCREEN_HEIGHT)

#player position
pos_x = int(0.5 * SCREEN_WIDTH/grid.size()[0] - PLAYER_W/2)
pos_y = int(0.5 * SCREEN_WIDTH/grid.size()[1] - PLAYER_H/2)
trail = []

wall_pixels = get_wall_pixels(grid, WALL_SIZE, gap, SCREEN_WIDTH, SCREEN_HEIGHT)

won = False
run = True

while run:
    while not won and run:
        pygame.time.delay(TIME_DELAY)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False

        keys = pygame.key.get_pressed()
        
        # move player
        if keys[pygame.K_LEFT]:
            if(len(set(get_rect_pixels(pos_x-VELOCITY,pos_y, PLAYER_H, PLAYER_W)).intersection(set(wall_pixels)))== 0):
                pos_x -= VELOCITY
            
        if keys[pygame.K_RIGHT]:
            if(len(set(get_rect_pixels(pos_x+VELOCITY,pos_y, PLAYER_H, PLAYER_W)).intersection(set(wall_pixels)))== 0):
                pos_x += VELOCITY

        if keys[pygame.K_UP]:
            if(len(set(get_rect_pixels(pos_x,pos_y-VELOCITY, PLAYER_H, PLAYER_W)).intersection(set(wall_pixels)))== 0):
                pos_y -= VELOCITY

        if keys[pygame.K_DOWN]:
            if(len(set(get_rect_pixels(pos_x,pos_y+VELOCITY, PLAYER_H, PLAYER_W)).intersection(set(wall_pixels)))== 0):
                pos_y += VELOCITY

        # check if player got in target area
        if(pos_x in range(target_x[0], target_x[1]) and pos_y in range(target_y[0], target_y[1])):
            won = 1

        # update player's trail
        if((pos_x,pos_y) not in trail):
            trail.append((int(pos_x),pos_y))
        
        # draw screen elements
        win.fill(BACKGROUND_C) 
        pygame.draw.rect(win, TARGET_C, (target_x[0], target_y[0], gap, gap))
        for x1,y1 in trail:
            pygame.draw.rect(win, TRAIL_C, (x1,y1,PLAYER_W, PLAYER_H))
        for x1,y1 in wall_pixels:
            pygame.draw.rect(win, WALL_C, (x1,y1,1,1))
        pygame.draw.rect(win, PLAYER_C, (pos_x, pos_y, PLAYER_W, PLAYER_H)) 
        pygame.display.update() 

    while won and run:
        pygame.time.delay(TIME_DELAY)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False

            if event.type == pygame.MOUSEBUTTONDOWN:
                
                mouse = pygame.mouse.get_pos()
                if((mouse[0], mouse[1]) in get_rect_pixels(EASY_BUTTON_X, EASY_BUTTON_Y, BUTTON_H, BUTTON_W)):
                    won = 0
                    grid = Grid(GRID_SIZE_EASY, GRID_SIZE_EASY).labyrinth()

                if((mouse[0], mouse[1]) in get_rect_pixels(MED_BUTTON_X, MED_BUTTON_Y, BUTTON_H, BUTTON_W*1.5)):
                    won = 0
                    grid = Grid(GRID_SIZE_MEDIUM, GRID_SIZE_MEDIUM).labyrinth()

                if((mouse[0], mouse[1]) in get_rect_pixels(HARD_BUTTON_X, HARD_BUTTON_Y, BUTTON_H, BUTTON_W)):
                    won = 0
                    grid = Grid(GRID_SIZE_HARD, GRID_SIZE_HARD).labyrinth()

                if(won == 0):
                    gap = int((SCREEN_WIDTH-(grid.size()[0]*(WALL_SIZE-1)))/grid.size()[0])
                    target_x = (SCREEN_WIDTH - gap, SCREEN_WIDTH) 
                    target_y = (SCREEN_HEIGHT - gap, SCREEN_HEIGHT)

                    #player position
                    pos_x = int(0.5 * SCREEN_WIDTH/grid.size()[0] - PLAYER_W/2)
                    pos_y = int(0.5 * SCREEN_WIDTH/grid.size()[1] - PLAYER_H/2)
                    trail = []

                    wall_pixels = get_wall_pixels(grid, WALL_SIZE, gap, SCREEN_WIDTH, SCREEN_HEIGHT)



        text = FONT.render('congrats!', True, FONT_C)
        win.blit(text, (SCREEN_WIDTH/3, SCREEN_HEIGHT/3))
        text = FONT.render('new game?', True, FONT_C)
        win.blit(text, (SCREEN_WIDTH/3, SCREEN_HEIGHT/3 + FONT_SIZE*1.2))


        pygame.draw.rect(win, BUTTON_C, (EASY_BUTTON_X, EASY_BUTTON_Y, BUTTON_W, BUTTON_H))
        text = FONT.render('easy', True, FONT_C)
        win.blit(text, (EASY_BUTTON_X+15, EASY_BUTTON_Y + 5))

        pygame.draw.rect(win, BUTTON_C, (MED_BUTTON_X, MED_BUTTON_Y, BUTTON_W*1.5, BUTTON_H)) 
        text = FONT.render('medium', True, FONT_C)
        win.blit(text, (EASY_BUTTON_X+ 1.5*BUTTON_W+10, MED_BUTTON_Y + 5))

        pygame.draw.rect(win, BUTTON_C, (HARD_BUTTON_X, HARD_BUTTON_Y, BUTTON_W, BUTTON_H)) 
        text = FONT.render('hard', True, FONT_C)
        win.blit(text, (EASY_BUTTON_X + 3.5*BUTTON_W +15, HARD_BUTTON_Y + 5))




        pygame.display.update()

pygame.quit()