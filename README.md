# Labyrinth game

Simple labyrinth game created with pygame. Kurskal's algorithm used for generating a maze.
![image.png](./image.png)

## Labyrinth

New labyrinth is generated each time a game is started. 
Labyrinths are generated using Kruskal's random minimum spanning tree algorithm.
See https://weblog.jamisbuck.org/2011/1/3/maze-generation-kruskal-s-algorithm 

#### Kruskal's algorithm
Each cell in a grid acts like a node in a graph. Each wall in grid acts like a lack of edge between two nodes.
Pseudocode:
```
raise walls between every 2 neighbouring cells

while some pairs are unvisited:
  c1, c2 = random neighbouring cells
  if wall in between c1,c2:
    if you "can't walk" from c1 to c2:
      remove wall
    else:
      wall is staying
      do not visit pair c1,c2 anymore
```
## Game

Firstly a very simple game (5x5 grid) is loaded. When completed player gets to choose difficulty level ranging from 10x10 grid to 25x25). Player starts at the upper left corner of a screen and has to reach bottom right corner. When moving, trail is left to indicate former movements.

### executable

You can create .exe file using PyInstaller. 
Something like `py -m PyInstaller --onefile --noconsole game.py --hidden-import game` will do.
