from itertools import combinations
import random

class Cell():
    """acts like a node in graph
    x, y: coordinates
    group: all cells you can walk to (no walls in between)
    """
    def __init__(self, x: int, y: int):
        self.x = x
        self.y = y
        self.group = [self]

    def __str__(self):
        return "[" + str(self.x) + ", " + str(self.y) + "]"

    def get_group(self):
        return self.group

    def set_group(self, g):
        self.group = g


def unionize_cells(c1: Cell, c2: Cell):
    """unionizes groups of cells that weren't in the same group
    updates all the cells in question
    """
    g1 = c1.get_group()
    g2 = c2.get_group()
    g = list(set(g1 + g2))

    for cell in g:
        cell.set_group(g)


class Wall():
    """data structure that acts like a lack of node in graph"""
    def __init__(self, c1: Cell, c2: Cell):
        self.c1 = c1
        self.c2 = c2

    def is_removable(self) -> bool:
        """returns true if cells belong to different groups"""
        return self.c1 not in self.c2.get_group()

class Grid():
    """grid of cells and walls"""

    def __init__(self, grid_w = 10, grid_h = 10):
        """firstly all possible walls are risen"""
        self.cells = []
        self.removable_walls = []
        self.unremovable_walls = []
        self.width = grid_w
        self.height = grid_h

        #generate all cells
        for x in range(0, grid_w):
            for y in range(0, grid_h):
                self.cells.append(Cell(x,y))

        #generate all walls
        for c1, c2 in combinations(self.cells, 2):
            if (c1.x == c2.x and abs(c1.y-c2.y) == 1 or c1.y == c2.y and abs(c1.x - c2.x) == 1): #if neighbours -> raise wall
                self.removable_walls.append(Wall(c1, c2))

    def size(self):
        return self.width, self.height

    def labyrinth(self):
        """generates labyrinth using kruskals random algo"""
        
        while(self.removable_walls):
            wall = random.choice(self.removable_walls)

            if(wall.is_removable()):
                unionize_cells(wall.c1, wall.c2)
                self.removable_walls.remove(wall)

            else:
                self.unremovable_walls.append(wall)
                self.removable_walls.remove(wall)
        return self