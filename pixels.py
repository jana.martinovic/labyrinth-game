from labyrinth import Grid

def get_wall_pixels(grid, wall_size, gap, screen_width, screen_height):
    """returns list of pixels for walls"""
    wall_pixels = []

    for wall in grid.unremovable_walls:
        if(wall.c1.y == wall.c2.y): #horizontal wall 
            start = wall.c1.y * gap + wall.c1.y * (wall_size-1) 
            end = start + gap + wall_size

            y_const = wall.c2.x * gap + wall.c2.x * (wall_size-1) 

            for pixel_x in range(start, end):
                wall_pixels.extend([(pixel_x, y_const+i) for i in range(0,wall_size)]) 

        else: #vertical
            start = wall.c1.x * gap + wall.c1.x * (wall_size - 1)
            end = start + gap + wall_size

            x_const = wall.c2.y * gap + wall.c2.y * (wall_size - 1)

            for pixel_y in range(start, end):
                wall_pixels.extend([(x_const+i, pixel_y) for i in range(0,wall_size)])

    # add window borders
    for i in range(0, wall_size):
        wall_pixels.extend([(x,0+i) for x in range(0, screen_width)])
        wall_pixels.extend([(x,screen_height-i-1) for x in range(0, screen_width)])
        wall_pixels.extend([(0+i,y) for y in range(0, screen_height)])
        wall_pixels.extend([(screen_width-i-1,y) for y in range(0, screen_height)])

    return wall_pixels

def get_rect_pixels(pos_x, pos_y, height, width):
    """returns list of pixels for player"""
    player_pixels = []
    height = int(height)
    width = int(width)
    pos_x = int(pos_x)
    pos_y = int(pos_y)

    for x in range(pos_x, pos_x + width):
        for y in range(pos_y, pos_y + height):
            player_pixels.append((x,y))

    return player_pixels